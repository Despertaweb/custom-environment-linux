# Custom Environment - Linux 

## Getting started

 ### 1. ZSH

Bash is the default Ubuntu shell, but ZSH is in another league with his productivity boosts. So, we are going to install ZSH over Bash.
Install


```
sudo apt install zsh
```

Verify the installed version.

```
zsh --version
```
Close and reopen the terminal


**1.1 - Oh my zsh!**
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

**1.2 - ZSH Theme** - [PowerLevel10k](https://github.com/romkatv/powerlevel10k)
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

On the ~/.zshrc file add this additional configuration

```
ZSH_THEME="powerlevel10k/powerlevel10k"
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(history)
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1

export LS_COLORS="rs=0:no=00:mi=00:mh=00:ln=01;36:or=01;31:di=01;34:ow=04;01;34:st=34:tw=04;34:pi=01;33:so=01;33:do=01;33:bd=01;33:cd=01;33:su=01;35:sg=01;35:ca=01;35:ex=01;32:"
```


Restart the terminal and type:
```
p10k configure
```

---
## NVM - Node Version Manager
Install the Node Version manager:
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```
Retart Terminal & Install Node Version:

```
nvm install v<node-version>
// example: nvm install v18
```

---
## YARN 
```
corepack enable
```

---
## SSH KEYS
**Generate SSH KEY**
```
ssh-keygen -t ed25519 -C "<comment>"
```

**Add SSH KEY to gitlabe account**
Copy the key:
```
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
Paste it there: [GitLab Keys page](https://gitlab.com/-/profile/keys)

----
## Git global config
```
git config --global user.email «usuari@domini.com»
git config --global user.name «El vostre nom»
```

---
Ready to party!